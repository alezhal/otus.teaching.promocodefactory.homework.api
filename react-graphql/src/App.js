import './App.css';
import { useEffect, useState } from 'react';
import * as Constants from './constants';
import axios from 'axios';
import PostForm from './components/PostForm';
import 'devextreme/dist/css/dx.light.css';
import { DataGrid } from 'devextreme-react';

function App() {

  const [data, setData] = useState({ customers: [] });
  const [didUpdate, setDidUpdate] = useState({ didUpdate: false });

  useEffect(() => {
    async function fetchData() {
      const queryResult = await axios.post(
        process.env.REACT_APP_GRAPHQL_API, {
        query: Constants.GET_CUSTOMERS_QUERY
      }
      );
      const result = queryResult.data.data;
      setData({ customers: result.customers })
      return result.customers;
    };

    fetchData();
  }, [didUpdate]);

  const setStateOfParent = (name) => {
    setDidUpdate(name);
  }

  return (
    <div className="App">
      <h2>Create a customer</h2>
      <PostForm setStateOfParent={setStateOfParent} />
      <h2>List of Customers</h2>
      <DataGrid
        dataSource={data.customers}
        keyExpr={data.customers.id}>
      </DataGrid>
    </div>
  );
}

export default App;
