export const WEB_API_CUSTOMER_CREATE_URL = "http://localhost:5000/api/v1/Customers";
export const GRAPHQL_API = "http://localhost:5000/graphql";
export const GET_CUSTOMERS_QUERY = `
query customer {
    customers {
      id
      email
      fullName
    }
  }
`;
export const WEB_API_CUSTOMER_HUB = "http://localhost:5000/customers";